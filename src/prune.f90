! Copyright (c) 2022 Archaeologic, Inc., Brad Richardson
! This software was developed for the U.S. Nuclear Regulatory Commission (US NRC) under contract # 31310020D0006:
! "Technical Assistance in Support of NRC Nuclear Regulatory Research for Materials, Waste, and Reactor Programs"
module prune
    use exceptional_numbers, only: is_infinity, is_nan, is_negative, is_zero
    use iso_varying_string, only: &
            varying_string, assignment(=), operator(//), char, var_str
    use sortff, only: reverse_sorted_order
    use strff, only: add_hanging_indentation, join, to_string, NEWLINE

    implicit none
    private

    public :: report_differences

    interface report_differences
        module procedure report_integer_differences
        module procedure report_real_differences
        module procedure report_double_precision_differences
    end interface
contains
    pure function report_integer_differences( &
            expected, &
            actual, &
            absolute_tolerance, &
            relative_tolerance, &
            name, &
            max_reported) &
            result(description)
        integer, intent(in) :: expected(..)
        integer, intent(in) :: actual(..)
        integer, intent(in) :: absolute_tolerance
        real, intent(in) :: relative_tolerance
        character(len=*), intent(in) :: name
        integer, intent(in) :: max_reported
        type(varying_string) :: description

        type :: difference_t
            integer, allocatable :: position(:)
            integer :: expected, actual
            integer :: absolute_difference
            real :: relative_difference
        end type

        type :: for_comparison_t
            integer, allocatable :: position(:)
            integer :: expected, actual
        end type

        type(difference_t), allocatable :: differences(:)

        if (all(shape(expected) == shape(actual))) then
            allocate(differences, source = differences_exceeding( &
                    expected, actual, absolute_tolerance, relative_tolerance))
            select rank (expected)
            rank (0)
                if (size(differences) > 0) then
                    description = difference_description(differences(1), name)
                else
                    description = ""
                end if
            rank default
                description = summary_of_at_most( &
                        max_reported, &
                        differences, &
                        name, &
                        " with shape (" // join(to_string(shape(actual)), ", ") &
                        // ") and size (" // to_string(size(actual)) // ")")
            end select
        else
            description = &
                    name // " had dimensions (" &
                    // join(to_string(shape(actual)), ", ") // ")" &
                    // " but should have had dimensions (" &
                    // join(to_string(shape(expected)), ", ") // ")"
        end if
    contains
        pure function differences_exceeding( &
                expected, &
                actual, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            integer, intent(in) :: expected(..)
            integer, intent(in) :: actual(..)
            integer, intent(in) :: absolute_tolerance
            real, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            integer :: i, j, k, l, m, n, o, p, q, r, s, t, u, v, w

            if (rank(expected) /= rank(actual)) then
                error stop "prune.differences_exceeding: arguments must have same rank, " &
                        // "but had ranks " // char(to_string(rank(expected))) &
                        // " and " // char(to_string(rank(actual)))
            end if
            if (.not.all(shape(expected) == shape(actual))) then
                error stop "prune.differences_exceeding: arguments must have same shape, " &
                        // "but had shapes (" // char(join(to_string(shape(expected)), ", ")) &
                        // ") and (" // char(join(to_string(shape(expected)), ", ")) // ")"
            end if
            if (size(expected) == 0) then
                allocate(differences(0))
                return
            end if
            select rank (expected_ => expected)
            rank (0)
                select rank (actual)
                rank (0)
                    differences = actual_comparison( &
                            for_comparing = [for_comparison_t( &
                                    position = [integer::], &
                                    expected = expected_, &
                                    actual = actual)], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (1)
                select rank (actual)
                rank (1)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    for_comparison_t( &
                                            position = [i], &
                                            expected = expected_(i), &
                                            actual = actual(i)), &
                                    i = 1, size(expected_))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (2)
                select rank (actual)
                rank (2)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j], &
                                            expected = expected_(i, j), &
                                            actual = actual(i, j)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (3)
                select rank (actual)
                rank (3)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k], &
                                            expected = expected_(i, j, k), &
                                            actual = actual(i, j, k)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (4)
                select rank (actual)
                rank (4)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l], &
                                            expected = expected_(i, j, k, l), &
                                            actual = actual(i, j, k, l)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (5)
                select rank (actual)
                rank (5)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m], &
                                            expected = expected_(i, j, k, l, m), &
                                            actual = actual(i, j, k, l, m)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (6)
                select rank (actual)
                rank (6)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n], &
                                            expected = expected_(i, j, k, l, m, n), &
                                            actual = actual(i, j, k, l, m, n)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (7)
                select rank (actual)
                rank (7)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o], &
                                            expected = expected_(i, j, k, l, m, n, o), &
                                            actual = actual(i, j, k, l, m, n, o)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (8)
                select rank (actual)
                rank (8)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p], &
                                            expected = expected_(i, j, k, l, m, n, o, p), &
                                            actual = actual(i, j, k, l, m, n, o, p)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (9)
                select rank (actual)
                rank (9)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q), &
                                            actual = actual(i, j, k, l, m, n, o, p, q)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (10)
                select rank (actual)
                rank (10)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (11)
                select rank (actual)
                rank (11)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (12)
                select rank (actual)
                rank (12)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (13)
                select rank (actual)
                rank (13)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (14)
                select rank (actual)
                rank (14)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (15)
                select rank (actual)
                rank (15)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v, w], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                                    w = 1, size(expected_, dim = 15))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            end select
        end function

        pure function actual_comparison( &
                for_comparing, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            type(for_comparison_t), intent(in) :: for_comparing(:)
            integer, intent(in) :: absolute_tolerance
            real, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            type(difference_t), allocatable :: all_differences(:)
            logical, allocatable :: exceeding_tolerances(:)
            integer :: i

            associate(num_comparisons => size(for_comparing))
                allocate(all_differences(num_comparisons))
                do concurrent (i = 1:num_comparisons)
                    block
                        real :: relative_difference

                        if (for_comparing(i)%expected == 0) then
                            if (for_comparing(i)%actual == 0) then
                                relative_difference = 0.0
                            else
                                relative_difference = 1.0 ! huge(0.0) avoid triggering FPEs
                            end if
                        else
                            relative_difference = &
                                    (for_comparing(i)%actual - for_comparing(i)%expected) &
                                    / real(abs(for_comparing(i)%expected))
                        end if
                        all_differences(i) = difference_t( &
                                position = for_comparing(i)%position, &
                                expected = for_comparing(i)%expected, &
                                actual = for_comparing(i)%actual, &
                                absolute_difference = for_comparing(i)%actual - for_comparing(i)%expected, &
                                relative_difference = relative_difference)
                    end block
                end do
                exceeding_tolerances = [( &
                        abs(all_differences(i)%absolute_difference) > absolute_tolerance &
                        .and. abs(all_differences(i)%relative_difference) > relative_tolerance, &
                        i = 1, num_comparisons)]
            end associate
            differences = pack(all_differences, exceeding_tolerances)
        end function

        elemental function difference_description(difference, name) result(description)
            type(difference_t), intent(in) :: difference
            character(len=*), intent(in) :: name
            type(varying_string) :: description

            if (allocated(difference%position)) then
                description = &
                        name &
                        // merge( &
                                "(" // join(to_string(difference%position), ", ") // ")", &
                                var_str(""), &
                                size(difference%position) > 0) &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            else
                description = &
                        name &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            end if
        end function

        pure function summary_of_at_most( &
                number_of, differences, name, shape_description) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string), intent(in) :: shape_description
            type(varying_string) :: summary

            if (size(differences) > 0) then
                summary = &
                        name // shape_description // " had " // to_string(size(differences)) // " differences." // NEWLINE &
                        // top_relative_differences(number_of, differences, name) // NEWLINE &
                        // top_absolute_differences(number_of, differences, name)
            else
                summary = ""
            end if
        end function

        pure function top_relative_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%relative_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by relative:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by relative:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function

        pure function top_absolute_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%absolute_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by absolute:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by absolute:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function
    end function

    pure function report_real_differences( &
            expected, &
            actual, &
            absolute_tolerance, &
            relative_tolerance, &
            name, &
            max_reported) &
            result(description)
        real, intent(in) :: expected(..)
        real, intent(in) :: actual(..)
        real, intent(in) :: absolute_tolerance
        real, intent(in) :: relative_tolerance
        character(len=*), intent(in) :: name
        integer, intent(in) :: max_reported
        type(varying_string) :: description

        type :: difference_t
            integer, allocatable :: position(:)
            real :: expected, actual
            real :: absolute_difference
            real :: relative_difference
        end type

        type :: for_comparison_t
            integer, allocatable :: position(:)
            real :: expected, actual
        end type

        type(difference_t), allocatable :: differences(:)

        if (all(shape(expected) == shape(actual))) then
            allocate(differences, source = differences_exceeding( &
                    expected, actual, absolute_tolerance, relative_tolerance))
            select rank (expected)
            rank (0)
                if (size(differences) > 0) then
                    description = difference_description(differences(1), name)
                else
                    description = ""
                end if
            rank default
                description = summary_of_at_most( &
                        max_reported, &
                        differences, &
                        name, &
                        " with shape (" // join(to_string(shape(actual)), ", ") &
                        // ") and size (" // to_string(size(actual)) // ")")
            end select
        else
            description = &
                    name // " had dimensions (" &
                    // join(to_string(shape(actual)), ", ") // ")" &
                    // " but should have had dimensions (" &
                    // join(to_string(shape(expected)), ", ") // ")"
        end if
    contains
        pure function differences_exceeding( &
                expected, &
                actual, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            real, intent(in) :: expected(..)
            real, intent(in) :: actual(..)
            real, intent(in) :: absolute_tolerance
            real, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            integer :: i, j, k, l, m, n, o, p, q, r, s, t, u, v, w

            if (rank(expected) /= rank(actual)) then
                error stop "prune.differences_exceeding: arguments must have same rank, " &
                        // "but had ranks " // char(to_string(rank(expected))) &
                        // " and " // char(to_string(rank(actual)))
            end if
            if (.not.all(shape(expected) == shape(actual))) then
                error stop "prune.differences_exceeding: arguments must have same shape, " &
                        // "but had shapes (" // char(join(to_string(shape(expected)), ", ")) &
                        // ") and (" // char(join(to_string(shape(expected)), ", ")) // ")"
            end if
            if (size(expected) == 0) then
                allocate(differences(0))
                return
            end if
            select rank (expected_ => expected)
            rank (0)
                select rank (actual)
                rank (0)
                    differences = actual_comparison( &
                            for_comparing = [for_comparison_t( &
                                    position = [integer::], &
                                    expected = expected_, &
                                    actual = actual)], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (1)
                select rank (actual)
                rank (1)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    for_comparison_t( &
                                            position = [i], &
                                            expected = expected_(i), &
                                            actual = actual(i)), &
                                    i = 1, size(expected_))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (2)
                select rank (actual)
                rank (2)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j], &
                                            expected = expected_(i, j), &
                                            actual = actual(i, j)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (3)
                select rank (actual)
                rank (3)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k], &
                                            expected = expected_(i, j, k), &
                                            actual = actual(i, j, k)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (4)
                select rank (actual)
                rank (4)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l], &
                                            expected = expected_(i, j, k, l), &
                                            actual = actual(i, j, k, l)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (5)
                select rank (actual)
                rank (5)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m], &
                                            expected = expected_(i, j, k, l, m), &
                                            actual = actual(i, j, k, l, m)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (6)
                select rank (actual)
                rank (6)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n], &
                                            expected = expected_(i, j, k, l, m, n), &
                                            actual = actual(i, j, k, l, m, n)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (7)
                select rank (actual)
                rank (7)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o], &
                                            expected = expected_(i, j, k, l, m, n, o), &
                                            actual = actual(i, j, k, l, m, n, o)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (8)
                select rank (actual)
                rank (8)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p], &
                                            expected = expected_(i, j, k, l, m, n, o, p), &
                                            actual = actual(i, j, k, l, m, n, o, p)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (9)
                select rank (actual)
                rank (9)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q), &
                                            actual = actual(i, j, k, l, m, n, o, p, q)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (10)
                select rank (actual)
                rank (10)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (11)
                select rank (actual)
                rank (11)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (12)
                select rank (actual)
                rank (12)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (13)
                select rank (actual)
                rank (13)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (14)
                select rank (actual)
                rank (14)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (15)
                select rank (actual)
                rank (15)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v, w], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                                    w = 1, size(expected_, dim = 15))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            end select
        end function

        pure function actual_comparison( &
                for_comparing, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            type(for_comparison_t), intent(in) :: for_comparing(:)
            real, intent(in) :: absolute_tolerance
            real, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            type(difference_t), allocatable :: all_differences(:)
            logical, allocatable :: exceeding_tolerances(:)
            integer :: i

            associate(num_comparisons => size(for_comparing))
                allocate(all_differences(num_comparisons))
                do concurrent (i = 1:num_comparisons)
                    block
                        real :: absolute_difference, relative_difference

                        if ( &
                                is_nan(for_comparing(i)%expected) &
                                .or. is_nan(for_comparing(i)%actual) &
                                .or. is_infinity(for_comparing(i)%expected) &
                                .or. is_infinity(for_comparing(i)%actual)) then
                            if ( &
                                    is_infinity(for_comparing(i)%actual) &
                                    .and. is_negative(for_comparing(i)%actual) &
                                    .and. (.not.is_infinity(for_comparing(i)%expected) &
                                            .or. .not.is_negative(for_comparing(i)%expected))) then
                                absolute_difference = -real_inf()
                                relative_difference = -real_inf()
                            else
                                absolute_difference = real_inf()
                                relative_difference = real_inf()
                            end if
                        else if (is_zero(for_comparing(i)%expected)) then
                            if (is_zero(for_comparing(i)%actual)) then
                                absolute_difference = 0.0
                                relative_difference = 0.0
                            else
                                absolute_difference = &
                                        for_comparing(i)%actual - for_comparing(i)%expected
                                relative_difference = &
                                        sign(real_inf(), absolute_difference)
                            end if
                        else
                            absolute_difference = &
                                    for_comparing(i)%actual - for_comparing(i)%expected
                            relative_difference = &
                                    (for_comparing(i)%actual - for_comparing(i)%expected) &
                                    / abs(for_comparing(i)%expected)
                        end if
                        all_differences(i) = difference_t( &
                                position = for_comparing(i)%position, &
                                expected = for_comparing(i)%expected, &
                                actual = for_comparing(i)%actual, &
                                absolute_difference = absolute_difference, &
                                relative_difference = relative_difference)
                    end block
                end do
                exceeding_tolerances = [( &
                        abs(all_differences(i)%absolute_difference) > absolute_tolerance &
                        .and. abs(all_differences(i)%relative_difference) > relative_tolerance, &
                        i = 1, num_comparisons)]
            end associate
            differences = pack(all_differences, exceeding_tolerances)
        end function

        elemental function difference_description(difference, name) result(description)
            type(difference_t), intent(in) :: difference
            character(len=*), intent(in) :: name
            type(varying_string) :: description

            if (allocated(difference%position)) then
                description = &
                        name &
                        // merge( &
                                "(" // join(to_string(difference%position), ", ") // ")", &
                                var_str(""), &
                                size(difference%position) > 0) &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            else
                description = &
                        name &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            end if
        end function

        pure function summary_of_at_most( &
                number_of, differences, name, shape_description) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string), intent(in) :: shape_description
            type(varying_string) :: summary

            if (size(differences) > 0) then
                summary = &
                        name // shape_description // " had " // to_string(size(differences)) // " differences." // NEWLINE &
                        // top_relative_differences(number_of, differences, name) // NEWLINE &
                        // top_absolute_differences(number_of, differences, name)
            else
                summary = ""
            end if
        end function

        pure function top_relative_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%relative_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by relative:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by relative:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function

        pure function top_absolute_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%absolute_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by absolute:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by absolute:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function
    end function

    pure function report_double_precision_differences( &
            expected, &
            actual, &
            absolute_tolerance, &
            relative_tolerance, &
            name, &
            max_reported) &
            result(description)
        double precision, intent(in) :: expected(..)
        double precision, intent(in) :: actual(..)
        double precision, intent(in) :: absolute_tolerance
        double precision, intent(in) :: relative_tolerance
        character(len=*), intent(in) :: name
        integer, intent(in) :: max_reported
        type(varying_string) :: description

        type :: difference_t
            integer, allocatable :: position(:)
            double precision :: expected, actual
            double precision :: absolute_difference
            double precision :: relative_difference
        end type

        type :: for_comparison_t
            integer, allocatable :: position(:)
            double precision :: expected, actual
        end type

        type(difference_t), allocatable :: differences(:)

        if (all(shape(expected) == shape(actual))) then
            allocate(differences, source = differences_exceeding( &
                    expected, actual, absolute_tolerance, relative_tolerance))
            select rank (expected)
            rank (0)
                if (size(differences) > 0) then
                    description = difference_description(differences(1), name)
                else
                    description = ""
                end if
            rank default
                description = summary_of_at_most( &
                        max_reported, &
                        differences, &
                        name, &
                        " with shape (" // join(to_string(shape(actual)), ", ") &
                        // ") and size (" // to_string(size(actual)) // ")")
            end select
        else
            description = &
                    name // " had dimensions (" &
                    // join(to_string(shape(actual)), ", ") // ")" &
                    // " but should have had dimensions (" &
                    // join(to_string(shape(expected)), ", ") // ")"
        end if
    contains
        pure function differences_exceeding( &
                expected, &
                actual, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            double precision, intent(in) :: expected(..)
            double precision, intent(in) :: actual(..)
            double precision, intent(in) :: absolute_tolerance
            double precision, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            integer :: i, j, k, l, m, n, o, p, q, r, s, t, u, v, w

            if (rank(expected) /= rank(actual)) then
                error stop "prune.differences_exceeding: arguments must have same rank, " &
                        // "but had ranks " // char(to_string(rank(expected))) &
                        // " and " // char(to_string(rank(actual)))
            end if
            if (.not.all(shape(expected) == shape(actual))) then
                error stop "prune.differences_exceeding: arguments must have same shape, " &
                        // "but had shapes (" // char(join(to_string(shape(expected)), ", ")) &
                        // ") and (" // char(join(to_string(shape(expected)), ", ")) // ")"
            end if
            if (size(expected) == 0) then
                allocate(differences(0))
                return
            end if
            select rank (expected_ => expected)
            rank (0)
                select rank (actual)
                rank (0)
                    differences = actual_comparison( &
                            for_comparing = [for_comparison_t( &
                                    position = [integer::], &
                                    expected = expected_, &
                                    actual = actual)], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (1)
                select rank (actual)
                rank (1)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    for_comparison_t( &
                                            position = [i], &
                                            expected = expected_(i), &
                                            actual = actual(i)), &
                                    i = 1, size(expected_))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (2)
                select rank (actual)
                rank (2)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j], &
                                            expected = expected_(i, j), &
                                            actual = actual(i, j)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (3)
                select rank (actual)
                rank (3)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k], &
                                            expected = expected_(i, j, k), &
                                            actual = actual(i, j, k)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (4)
                select rank (actual)
                rank (4)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l], &
                                            expected = expected_(i, j, k, l), &
                                            actual = actual(i, j, k, l)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (5)
                select rank (actual)
                rank (5)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m], &
                                            expected = expected_(i, j, k, l, m), &
                                            actual = actual(i, j, k, l, m)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (6)
                select rank (actual)
                rank (6)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n], &
                                            expected = expected_(i, j, k, l, m, n), &
                                            actual = actual(i, j, k, l, m, n)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (7)
                select rank (actual)
                rank (7)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o], &
                                            expected = expected_(i, j, k, l, m, n, o), &
                                            actual = actual(i, j, k, l, m, n, o)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (8)
                select rank (actual)
                rank (8)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p], &
                                            expected = expected_(i, j, k, l, m, n, o, p), &
                                            actual = actual(i, j, k, l, m, n, o, p)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (9)
                select rank (actual)
                rank (9)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q), &
                                            actual = actual(i, j, k, l, m, n, o, p, q)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (10)
                select rank (actual)
                rank (10)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (11)
                select rank (actual)
                rank (11)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (12)
                select rank (actual)
                rank (12)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (13)
                select rank (actual)
                rank (13)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (14)
                select rank (actual)
                rank (14)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            rank (15)
                select rank (actual)
                rank (15)
                    differences = actual_comparison( &
                            for_comparing = [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    [( &
                                    for_comparison_t( &
                                            position = [i, j, k, l, m, n, o, p, q, r, s, t, u, v, w], &
                                            expected = expected_(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w), &
                                            actual = actual(i, j, k, l, m, n, o, p, q, r, s, t, u, v, w)), &
                                    i = 1, size(expected_, dim = 1))], &
                                    j = 1, size(expected_, dim = 2))], &
                                    k = 1, size(expected_, dim = 3))], &
                                    l = 1, size(expected_, dim = 4))], &
                                    m = 1, size(expected_, dim = 5))], &
                                    n = 1, size(expected_, dim = 6))], &
                                    o = 1, size(expected_, dim = 7))], &
                                    p = 1, size(expected_, dim = 8))], &
                                    q = 1, size(expected_, dim = 9))], &
                                    r = 1, size(expected_, dim = 10))], &
                                    s = 1, size(expected_, dim = 11))], &
                                    t = 1, size(expected_, dim = 12))], &
                                    u = 1, size(expected_, dim = 13))], &
                                    v = 1, size(expected_, dim = 14))], &
                                    w = 1, size(expected_, dim = 15))], &
                            absolute_tolerance = absolute_tolerance, &
                            relative_tolerance = relative_tolerance)
                end select
            end select
        end function

        pure function actual_comparison( &
                for_comparing, &
                absolute_tolerance, &
                relative_tolerance) &
                result(differences)
            type(for_comparison_t), intent(in) :: for_comparing(:)
            double precision, intent(in) :: absolute_tolerance
            double precision, intent(in) :: relative_tolerance
            type(difference_t), allocatable :: differences(:)

            type(difference_t), allocatable :: all_differences(:)
            logical, allocatable :: exceeding_tolerances(:)
            integer :: i

            associate(num_comparisons => size(for_comparing))
                allocate(all_differences(num_comparisons))
                do concurrent (i = 1:num_comparisons)
                    block
                        double precision :: absolute_difference, relative_difference

                        if ( &
                                is_nan(for_comparing(i)%expected) &
                                .or. is_nan(for_comparing(i)%actual) &
                                .or. is_infinity(for_comparing(i)%expected) &
                                .or. is_infinity(for_comparing(i)%actual)) then
                            if ( &
                                    is_infinity(for_comparing(i)%actual) &
                                    .and. is_negative(for_comparing(i)%actual) &
                                    .and. (.not.is_infinity(for_comparing(i)%expected) &
                                            .or. .not.is_negative(for_comparing(i)%expected))) then
                                absolute_difference = -dbl_inf()
                                relative_difference = -dbl_inf()
                            else
                                absolute_difference = dbl_inf()
                                relative_difference = dbl_inf()
                            end if
                        else if (is_zero(for_comparing(i)%expected)) then
                            if (is_zero(for_comparing(i)%actual)) then
                                absolute_difference = 0.d0
                                relative_difference = 0.d0
                            else
                                absolute_difference = &
                                        for_comparing(i)%actual - for_comparing(i)%expected
                                relative_difference = &
                                        sign(dbl_inf(), absolute_difference)
                            end if
                        else
                            absolute_difference = &
                                    for_comparing(i)%actual - for_comparing(i)%expected
                            relative_difference = &
                                    (for_comparing(i)%actual - for_comparing(i)%expected) &
                                    / abs(for_comparing(i)%expected)
                        end if
                        all_differences(i) = difference_t( &
                                position = for_comparing(i)%position, &
                                expected = for_comparing(i)%expected, &
                                actual = for_comparing(i)%actual, &
                                absolute_difference = absolute_difference, &
                                relative_difference = relative_difference)
                    end block
                end do
                exceeding_tolerances = [( &
                        abs(all_differences(i)%absolute_difference) > absolute_tolerance &
                        .and. abs(all_differences(i)%relative_difference) > relative_tolerance, &
                        i = 1, num_comparisons)]
            end associate
            differences = pack(all_differences, exceeding_tolerances)
        end function

        elemental function difference_description(difference, name) result(description)
            type(difference_t), intent(in) :: difference
            character(len=*), intent(in) :: name
            type(varying_string) :: description

            if (allocated(difference%position)) then
                description = &
                        name &
                        // merge( &
                                "(" // join(to_string(difference%position), ", ") // ")", &
                                var_str(""), &
                                size(difference%position) > 0) &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            else
                description = &
                        name &
                        // " was " &
                        // to_string(difference%actual) // " but should have been " &
                        // to_string(difference%expected) // ", was different by " &
                        // to_string(difference%absolute_difference) &
                        // " (" // to_string(difference%relative_difference * 100.0) // "%)"
            end if
        end function

        pure function summary_of_at_most( &
                number_of, differences, name, shape_description) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string), intent(in) :: shape_description
            type(varying_string) :: summary

            if (size(differences) > 0) then
                summary = &
                        name // shape_description // " had " // to_string(size(differences)) // " differences." // NEWLINE &
                        // top_relative_differences(number_of, differences, name) // NEWLINE &
                        // top_absolute_differences(number_of, differences, name)
            else
                summary = ""
            end if
        end function

        pure function top_relative_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%relative_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by relative:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by relative:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function

        pure function top_absolute_differences(number_of, differences, name) result(summary)
            integer, intent(in) :: number_of
            type(difference_t), intent(in) :: differences(:)
            character(len=*), intent(in) :: name
            type(varying_string) :: summary

            type(varying_string), allocatable :: descriptions(:)
            type(varying_string) :: header

            associate(sort_indices => reverse_sorted_order(abs(differences%absolute_difference)))
                if (number_of >= size(differences)) then
                    header = "Differences by absolute:"
                    descriptions = difference_description(differences(sort_indices), name)
                else
                    header = "Top " // to_string(number_of) // " differences by absolute:"
                    descriptions = difference_description(differences(sort_indices(1:number_of)), name)
                end if
            end associate
            summary = add_hanging_indentation( &
                    header // NEWLINE // join(descriptions, NEWLINE), &
                    4)
        end function
    end function

    pure function dbl_inf()
        double precision :: dbl_inf

        character(len=:), allocatable :: inf_str

        inf_str = "Inf"
        read(inf_str, *) dbl_inf
    end function

    pure function real_inf()
        real :: real_inf

        character(len=:), allocatable :: inf_str

        inf_str = "Inf"
        read(inf_str, *) real_inf
    end function
end module
