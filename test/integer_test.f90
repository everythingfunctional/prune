! Copyright (c) 2022 Archaeologic, Inc., Brad Richardson
! This software was developed for the U.S. Nuclear Regulatory Commission (US NRC) under contract # 31310020D0006:
! "Technical Assistance in Support of NRC Nuclear Regulatory Research for Materials, Waste, and Reactor Programs"
module integer_test
    use prune, only: report_differences
    use iso_varying_string, only: varying_string
    use strff, only: split_at, to_string, NEWLINE
    use veggies, only: &
            result_t, &
            test_item_t, &
            assert_empty, &
            assert_equals, &
            assert_includes, &
            describe, &
            it

    implicit none
    private
    public :: test_differences
contains
    function test_differences() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "report_differences (for integer)", &
                [ it("provides no differences for identical arrays", check_identical) &
                , it("includes the number of differences", check_number_of_differences_included) &
                , it("properly limits the number of differences reported", check_lines_limited) &
                , it("can be used with any rank", check_any_rank) &
                ])
    end function

    function check_identical() result(result_)
        type(result_t) :: result_

        integer, parameter :: the_array(*) = [2, 3, 5, 7, 11]

        result_ = assert_empty(report_differences( &
                expected = the_array, &
                actual = the_array, &
                absolute_tolerance = 0, &
                relative_tolerance = epsilon(0.0), &
                name = "the_array", &
                max_reported = 10))
    end function

    function check_number_of_differences_included() result(result_)
        type(result_t) :: result_

        integer, parameter :: expected(*) = [2, 3, 5, 7, 11]
        integer, parameter :: actual(*) = [2, 4, 6, 8, 10]
        integer, parameter :: num_differences = 4

        result_ = assert_includes( &
                to_string(num_differences), &
                report_differences( &
                        expected = expected, &
                        actual = actual, &
                        absolute_tolerance = 0, &
                        relative_tolerance = epsilon(0.0), &
                        name = "the_array", &
                        max_reported = 10))
    end function

    function check_lines_limited() result(result_)
        type(result_t) :: result_

        integer, parameter :: NUM_ACTUAL_DIFFERENCES = 4
        integer, parameter :: NUM_REQUESTED = 3
        integer, parameter :: NUM_HEADER_LINES = 3
        integer, parameter :: expected(*) = [2, 3, 5, 7, 11]
        integer, parameter :: actual(*) = [2, 4, 6, 8, 10]
        type(varying_string) :: short_summary, long_summary

        short_summary = report_differences( &
                expected = expected, &
                actual = actual, &
                absolute_tolerance = 0, &
                relative_tolerance = epsilon(0.0), &
                name = "the_array", &
                max_reported = NUM_REQUESTED)
        long_summary = report_differences( &
                expected = expected, &
                actual = actual, &
                absolute_tolerance = 0, &
                relative_tolerance = epsilon(0.0), &
                name = "the_array", &
                max_reported = NUM_ACTUAL_DIFFERENCES+1)
        result_ = &
                assert_equals( &
                        NUM_REQUESTED*2 + NUM_HEADER_LINES, &
                        size(split_at(short_summary, NEWLINE)), &
                        short_summary) &
                .and.assert_equals( &
                        NUM_ACTUAL_DIFFERENCES*2 + NUM_HEADER_LINES, &
                        size(split_at(long_summary, NEWLINE)), &
                        long_summary)
    end function

    function check_any_rank() result(result_)
        type(result_t) :: result_

        integer :: i

        result_ = &
            assert_includes( &
                "the_number", &
                report_differences( &
                        expected = 2, &
                        actual = 3, &
                        absolute_tolerance = 0, &
                        relative_tolerance = epsilon(0.0), &
                        name = "the_number", &
                        max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = [2], &
                            actual = [3], &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 2)]), &
                            actual = reshape([3], [(1, i = 1, 2)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 3)]), &
                            actual = reshape([3], [(1, i = 1, 3)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 4)]), &
                            actual = reshape([3], [(1, i = 1, 4)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 5)]), &
                            actual = reshape([3], [(1, i = 1, 5)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 6)]), &
                            actual = reshape([3], [(1, i = 1, 6)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 7)]), &
                            actual = reshape([3], [(1, i = 1, 7)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 8)]), &
                            actual = reshape([3], [(1, i = 1, 8)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 9)]), &
                            actual = reshape([3], [(1, i = 1, 9)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 10)]), &
                            actual = reshape([3], [(1, i = 1, 10)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 11)]), &
                            actual = reshape([3], [(1, i = 1, 11)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 12)]), &
                            actual = reshape([3], [(1, i = 1, 12)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 13)]), &
                            actual = reshape([3], [(1, i = 1, 13)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 14)]), &
                            actual = reshape([3], [(1, i = 1, 14)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1)) &
            .and.assert_includes( &
                    "the_array", &
                    report_differences( &
                            expected = reshape([2], [(1, i = 1, 15)]), &
                            actual = reshape([3], [(1, i = 1, 15)]), &
                            absolute_tolerance = 0, &
                            relative_tolerance = epsilon(0.0), &
                            name = "the_array", &
                            max_reported = 1))
    end function
end module
