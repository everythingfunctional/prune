# prune

A library for generating a summary of the differences between two arrays.
Given two arrays of `integer`, `real` or `double precision` of any rank (including scalars),
tolerances to use, and the maximum number of differences to report,
produces a string summarizing the differences found.
